import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/servicios/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {


  usuario: UsuarioModel;
  recordarme = false;

  constructor(private auth: AuthService ,
              private router : Router) { }

  ngOnInit() { 

    this.usuario = new UsuarioModel();
  }

  onSubmit(form : NgForm) {

    if(form.invalid) {return ;}

    Swal.fire({
      text: 'Espere por favor',
      allowOutsideClick : false,
      icon: 'info'
  });
  Swal.showLoading();

    this.auth.nuevoUsuario(this.usuario)
    .subscribe( resp => {
      Swal.close();

      if(this.recordarme){
        localStorage.setItem('email',this.usuario.email);
      }

      this.router.navigateByUrl('/home');

    }, (err) => {
      Swal.fire({
        title: 'Error al autenticar',
        text: err.error.error.message,
        allowOutsideClick : false,
        icon: 'error'
    });
    });


  }

  

}
